# ExCalendar

A simple repository to visualize combination of my and **@OvermindDL1** idea:
> Not all Calendars use a year/month/day/hour/minute/second/etc... construct, so putting that as a base-most Date/DateTimestructs was a very very bad move. Each Calendar should have its ownDate/DateTime structs, which also allows easy matching on:
> ```elixir
> def do_blah(%Calendar.Gregorian{year: year, month: month, day: day}), do: gregorianStuff(year, month, day)
>
> def do_blah(%Calendar.Bloop{egrek: egrek, vreenoop: vreenoop}), do: bloopStuff(egrek, vreenoop)
> ```

with **José Valim** note (`&ExCalendar.Calendar.default/1`):
> Given the Gregorian Calendar is the international de facto and using it as a base is what makes sense for the huge majority of developers and the huge majority of times, it makes sense to prioritize its representation.

**TODO**: specify univalsal unit in docs.
