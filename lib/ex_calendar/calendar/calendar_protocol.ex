defprotocol ExCalendar.Calendar.CalendarProtocol do
  @moduledoc false

  @doc """
  Faster return time presentation by name.

      defmodule MyLib.MyCalendar do
        @time_presentations %{} # [...]

        Enum.each(@time_presentations, fn({name, value}) ->
          def time_presentation(unquote(name)), do: unquote(value)
        end)

        def time_presentations(), do: @time_presentations
      end

  Returns module.
  """
  def time_presentation(name)

  @doc """
  Returns map of selected by calendar time presentations.
  Example:

      %{
        date: MyLib.BusinessDate,
        date_time: MyLib.BusinessDateTime,
        # time: Excalendar. ...
      }
  """
  def time_presentations()
end
