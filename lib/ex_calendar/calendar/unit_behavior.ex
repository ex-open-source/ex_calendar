defmodule ExCalendar.Calendar.UnitBehavior do
  @doc """
  Convert universal unit to current unit.
  """
  @callback from_universal_unit(value)

  @doc """
  Convert current unit to universal unit.
  """
  @callback to_universal_unit(value)
end
