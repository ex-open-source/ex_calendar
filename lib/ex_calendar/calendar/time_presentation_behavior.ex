defprotocol ExCalendar.Calendar.TimePresentationBehavior do
  @moduledoc """
  This module describes methods to manipulating time representation.
  """

  @doc """
  Adds unit values to struct in format:
   [unit_name: value] where unit name is one of supported units.
  Returns modified time presentation struct or:
    {:error, :unsupported_units, units_list}
  """
  @callback add(time_presentation, units \\ %{})

  @doc """
  Same as `&add/1`, but raises exceptions instead of return error.
  """
  @callback add!(time_presentation, units \\ %{})

  @doc """
  Comparision of two same time presentations
   with different unit values (like: different number of days)
  Returns one of: `:lt`, `:eq`, `:gt`.
  """
  @callback compare(first_time_presentation, second_time_presentation)

  @doc """
  Removes unit values from struct in format:
    [unit_name: value] where unit name is one of supported units.
  Use `&to_universal_unit/1` to compare time presentations.
  Returns modified time presentation struct or:
    {:error, :unsupported_units, units_list}
  """
  @callback del(time_presentation, units \\ %{})

  @doc """
  Same as `&del/1`, but raises exceptions instead of return error.
  """
  @callback del!(time_presentation, units \\ %{})

  @doc """
  Returns differences between two same time presentations.
  Example return: `%{hours: 1, seconds: 3}`
  """
  @callback diff(first_time_presentation, second_time_presentation)

  @doc """
  Returns differences between two same time presentations in universal unit.
  """
  @callback diff_universal(first_time_presentation, second_time_presentation)

  @doc """
  Converts from universal unit to time presentation.
  """
  @callback from_universal_unit(value)

  @doc """
  Returns time presentation struct with units in format:
    [unit_name: value] where unit name is one of supported units
  or:
    {:error, :unsupported_units, units_list}
  """
  @callback new(time_presentation, units \\ %{})

  @doc """
  Same as `&new/1`, but raises exceptions instead of return error.
  """
  @callback new!(time_presentation, units \\ %{})

  @doc """
  Returns time presentation struct.
  Optional `opts` can be used for example to set timezone.
  """
  @callback now(opts \\ %{})

  @doc """
  Checks if given module is supported by time presentation.
  """
  @callback supported_unit?(module)

  @doc """
  Returns `Map` of supported units in format: `%{symbol: module}`.
  """
  @callback supported_units(time_presentation)

  @doc """
  Convert time presentation to printable format.
  """
  @callback to_string(time_presentation)

  @doc """
  Converts time presentation to universal unit.
  """
  @callback to_universal_unit(time_presentation)

  @doc """
  Translate map with possible invalid units to time representation.
  For example:

      translate_units(%{minutes: 0.5})
      %MyLib.MyTime{hours: 0, minutes: 0, seconds: 30}

  Returns:
  `{:ok, result}`
    if success or
  `{:error, :unknown_unit}`
    if passed unit that time representation don't know or
  `{:error, :invalid_value}`
    if passed unit with bad value type (for example `String`) or negative value
  """
  @callback translate_units(units \\ %{})

  @doc """
  Same as `&translate_units/1`, but raises exceptions instead of return error.
  """
  @callback translate_units!(units \\ %{})
end
