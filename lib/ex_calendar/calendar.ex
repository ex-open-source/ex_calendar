defmodule ExCalendar.Calendar do
  @moduledoc false

  @doc """
  TODO: add default (Gregorian) implementation and replace nil with it
  Returns default calendar implementation struct.
  Example struct:

      %MyLib.MyCalendar{
        name: "My Gregorian calendar implementation",
        urls: %{wikipedia: "https://en.wikipedia.org/wiki/Gregorian_calendar"}
      }

  As **José Valim** wrote:
  > Given the Gregorian Calendar is the international de facto and using it as a base is what makes sense for the huge majority of developers and the huge majority of times, it makes sense to prioritize its presentation.
  """
  def default(),
      do: Application.get_env(:elixir, :default_calendar, nil)

  @doc """
  List all known calendar protocol implementations (`List` of structs).
  """
  def list() do
    path = :code.lib_dir(:ex_calendar, :ebin)
    Protocol.extract_impls(Calendar.CalendarProtocol, [path])
  end

  @doc """
  Returns a time presentation struct
  """
  def time_presentation(name),
      do: apply(default(), :time_presentation, name)
  def time_presentation(calendar, name),
      do: apply(calendar.__struct__, :time_presentation, name)

  @doc """
  Returns all selected by calendar time presentation structs
  Example format: %{date: Date, date_time: NaiveDateTime, time: Time}
  """
  def time_presentations(),
      do: apply(default(), :time_presentations)
  def time_presentations(calendar),
      do: apply(calendar.__struct__, :time_presentations)
end
